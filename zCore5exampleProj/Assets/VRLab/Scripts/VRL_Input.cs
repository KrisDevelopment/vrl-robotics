﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRL;

public class VRL_Input : MonoBehaviour, iListener {

	public VRL_Module module;
	public int inputId = 0;
	 
	public void Signal () {
		if(!module) return;
		if(!SceneManager.IsPlaying()) return;
		module.InputVal(new VRLVal("ping", 1f, true), inputId);
	}
	
	public void Signal (bool b){
		if(!module) return;
		if(!SceneManager.IsPlaying()) return;
		module.InputVal(new VRLVal(
				(b) ? "true" : "false",
				(b) ? 1f : 0f,
				b),
			inputId);
	}
	
	public void Signal (float f){
		if(!module) return;
		if(!SceneManager.IsPlaying()) return;
		module.InputVal(new VRLVal(
				"" + f,
				f,
				(f != 0f) ? true : false),
			inputId);
	}
	
	public void Signal (string str){
		if(!module) return;
		if(!SceneManager.IsPlaying()) return;
		module.InputVal(new VRLVal(
				str,
				1f,
				true),
			inputId);
	}
	
	public void Signal (VRLVal v){
		if(!module) return;
		if(!SceneManager.IsPlaying()) return;
		module.InputVal(v, inputId);
	}
	
	public Vector3 GetPosition(){
		return transform.position;
	}
}
