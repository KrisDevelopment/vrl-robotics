﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRL;

public class VRL_NodeSpace : MonoBehaviour {
	public static VRL_NodeSpace instance;
	public static bool isOpen = false;
	
	private string nodeLibPath = "VRL/NodeLibrary";
	public GameObject[] oNodeLibrary;
	public GameObject oDefaultNode;
	
	public GameObject
		oNodeGroup,
		oMaskParent;

	private VRL_Node[] nodes;
	private NodeData nodeData; //node data retrieved in a show call
	
	void Awake (){
		instance = this;
		//load node library from resource path
		Object[] _objNodeLib = Resources.LoadAll(nodeLibPath, typeof(GameObject));
		oNodeLibrary = new GameObject[_objNodeLib.Length];
		for(int i = 0; i < _objNodeLib.Length; oNodeLibrary[i] = (GameObject) _objNodeLib[i], i++);
	}
	
	public static void Show (ref NodeData nd) {
		if(isOpen){
			Debug.Log("[VRL_NodeSpace ERROR] A node view is already open!");
			return;
		}
		
		isOpen = true;
		Menu.SetIndex(Menues.NodeEditor);
		instance.nodeData = nd;
		instance.DisplayToggle(true);
		
		//TODO: load nodes
		//testing -->
		instance.AddNode(0);
	}
	
	public static void Hide () {
		isOpen = false;
		instance.DisplayToggle(false);
		if(instance.nodes != null)
			for(int i = 0; i < instance.nodes.Length; i++)
				instance.nodes[i].Delete();
		instance.nodes = new VRL_Node[0];
	}
	
	public VRL_Node AddNode (int nodeId){
		return AddNode(nodeId, oMaskParent.transform.position);
	}
	
	private VRL_Node AddNode (int nodeId, Vector3 pos) { //--master method
		if(oNodeLibrary == null){
			Debug.Log("[Node Space ERROR] node types empty!");
			return null;
		}
		
		GameObject _instance = Instantiate((oNodeLibrary[nodeId]) ? oNodeLibrary[nodeId] : oDefaultNode, pos, oMaskParent.transform.rotation);
		_instance.transform.parent = oMaskParent.transform;
		VRL_Node _node = _instance.GetComponent<VRL_Node>() as VRL_Node;
		
		//resize nodes
		if(nodes == null)
			nodes = new VRL_Node[0];
		//-expand and fill
		VRL_Node[] _nodesTemp = new VRL_Node[nodes.Length];
		for(uint i = 0; i < _nodesTemp.Length; _nodesTemp[i] = nodes[i], i++);
		nodes = new VRL_Node[_nodesTemp.Length + 1];
		for(uint i = 0; i < _nodesTemp.Length; nodes[i] = _nodesTemp[i], i++);
		nodes[nodes.Length - 1] = _node;
		
		return _node;
	}
	
	
	private void DisplayToggle (bool state) {
		oNodeGroup.SetActive(state);
	}
}
