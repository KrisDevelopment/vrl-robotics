﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRL;

public class VRL_Node : MonoBehaviour, iNode {
	//setup
	[Header("VRL_Node:")]
	public NodeType nodeType;
	public Text oName;
	public NodeInformation nodeInformation;
	
	void Start () {
		oName.text = name;
	}
	
	public void SetNodeInformation (NodeInformation ni) {
		nodeInformation = ni;
	}
	
	public void Delete() {
		Destroy(gameObject);
	}
	
	public virtual NodeType GetNodeType () {
		return nodeType;
	}
	
	public virtual GameObject GetGameObject(){
		return gameObject;
	}
}
