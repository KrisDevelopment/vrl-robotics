﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRL_ObjectBase : MonoBehaviour {
	protected bool
		wasKinematic = false,
		isPreview = false;
	protected int originalLayer;
	protected Material
		originalMaterial = null,
		previewMaterial = null;
	public Collider primaryCollider;
	
	
	public void MakeKinematic (bool b) {
		if(GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().isKinematic = (b) ? true : wasKinematic;
	}
	
	public void CollisionToggle (bool b) {
		if(GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().detectCollisions = b;
	}
	
	public virtual void MakeMovable (bool b){
		gameObject.layer = (b) ? LayerMask.NameToLayer("Movable") : originalLayer;
	}
	
	public bool IsPreview(){
		return isPreview;
	}
	
	public virtual void MakePreview(bool b){
		if(b && !isPreview){
			Renderer _renderer = GetComponent<Renderer>() as Renderer;
			if(_renderer)
				_renderer.material = previewMaterial;
			if(primaryCollider)
				primaryCollider.enabled = false;
			MakeKinematic(true);
			MakeMovable(true);
			isPreview = true;
		}
		if(!b && isPreview){
			Renderer _renderer = GetComponent<Renderer>() as Renderer;
			if(_renderer)
				_renderer.material = originalMaterial;
			if(primaryCollider)
				primaryCollider.enabled = true;
			MakeKinematic(false);
			MakeMovable(false);
			isPreview = false;
		}
	}
	
	public virtual void OnStart () {
		originalLayer = gameObject.layer;
		Renderer _renderer = GetComponent<Renderer>() as Renderer;
		if(_renderer)
			originalMaterial = _renderer.material;
		if(GetComponent<Rigidbody>())
			wasKinematic = GetComponent<Rigidbody>().isKinematic;
		primaryCollider = (Collider) GetComponent<Collider>();
		previewMaterial = (Material) Resources.Load("VRL/Materials/PreviewMaterial");
	}
}
