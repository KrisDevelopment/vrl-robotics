﻿namespace VRL { 
	public static class VRLu { //VR Lab Utility
		public static UnityEngine.Quaternion  GetRotationOffset (UnityEngine.Transform source , UnityEngine.Transform affected){
			return GetRotationOffset(source.rotation, affected.rotation);
		}

		public static UnityEngine.Quaternion  GetRotationOffset (UnityEngine.Quaternion source , UnityEngine.Quaternion affected){
			//source is always the forward facing vector and the offset is how much does the affected need to rotate in order to reach that forward vector
			UnityEngine.Quaternion offset = UnityEngine.Quaternion.Inverse(source) * affected;
			return offset;
		}
	}
}