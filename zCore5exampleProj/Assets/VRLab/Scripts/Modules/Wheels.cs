﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRL;

public class Wheels : VRL_Module {
	
	public WheelCollider
		wheelcFR,
		wheelcFL,
		wheelcRR,
		wheelcRL;
	public Transform
		wheelFR,
		wheelFL,
		wheelRR,
		wheelRL;
	public float motorForce = 50;
	public float maxSteerAngle = 30;
	
	private float
		input_vertical = 0f,
		input_horizontal = 0f;
	private float steerAngle = 0f;
	
	public override void InputVal (VRLVal val, int inputId){
		if(inputId == (int)DriveInput.Forward)
			input_vertical = val.floatVal;
		if(inputId == (int)DriveInput.Sideways)
			input_horizontal = val.floatVal;
	}
	
	void Update () {
		Accelerate();
		Steer();
		UpdateWheelPose();
		ClearInput();
	}
	
	private void Accelerate () {
		float _torque = input_vertical * motorForce;
		wheelcFL.motorTorque = _torque;
		wheelcFR.motorTorque = _torque;
	}
	
	private void Steer() {
		steerAngle = maxSteerAngle * input_horizontal;
		wheelcFL.steerAngle = steerAngle;
		wheelcFR.steerAngle = steerAngle;
	}
	
	private void UpdateWheelPose (){
		UpdateWheelPose(wheelcFL, ref wheelFL);
		UpdateWheelPose(wheelcFR, ref wheelFR);
		UpdateWheelPose(wheelcRL, ref wheelRL);
		UpdateWheelPose(wheelcRR, ref wheelRR);
	}
	
	private void UpdateWheelPose (WheelCollider wc, ref Transform t) {
		Vector3 _wpos = transform.position;
		Quaternion _wrot = transform.rotation;
		wc.GetWorldPose(out _wpos, out _wrot);
		t.position = _wpos;
		t.rotation = _wrot;
	}
	
	private void ClearInput(){
		input_horizontal = 0f;
		input_vertical = 0f;
	}
}
