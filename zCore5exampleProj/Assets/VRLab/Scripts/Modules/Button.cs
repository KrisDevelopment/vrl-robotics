﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : VRL_Module {
	public bool state = false;
	
	void Update () {
		if(state)
			if(outputs != null)
				for(int i = 0; i < outputs.Length; i++)
					outputs[i].Signal();
	}
	
	public override void Interact () {
		state = !state;
	}
}
