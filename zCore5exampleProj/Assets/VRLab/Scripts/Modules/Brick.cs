﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRL;

public class Brick : VRL_Module {
	public NodeData nodeData;
	
	void Update () {
		//TODO: read node data and run operations from that
		//maybe use the turtle principle?
	}
	
	public override void Interact () {
		VRL_NodeSpace.Show(ref nodeData);
	}
}
