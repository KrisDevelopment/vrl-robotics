﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pinger : VRL_Module {
	
	void Update () {
		if(outputs != null){
			for(int i = 0; i < outputs.Length; i++)
				outputs[i].Signal();
		}
	}
}
