﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRL;

public class BipedLegs : VRL_Module {
	public Animator animator;
	private float
		turnSpeed = 1f,
		moveSpeed = 1f,
		input_vertical = 0f,
		input_horizontal = 0f;
	private CharacterController controller;
	
	void Start () {
		controller = GetComponent<CharacterController>() as CharacterController;
	}
	
	void Update () {
		animator.SetFloat("Vertical", input_vertical);
		animator.SetFloat("Horizontal", input_horizontal);
		
		Walk();
	}
	
	public override void InputVal (VRLVal val, int inputId){
		if(inputId == (int)DriveInput.Forward)
			input_vertical = val.floatVal;
		if(inputId == (int)DriveInput.Sideways)
			input_horizontal = val.floatVal;
	}
	
	private void Walk () {
		controller.Move(transform.forward * input_vertical * moveSpeed);
		
	}
}
