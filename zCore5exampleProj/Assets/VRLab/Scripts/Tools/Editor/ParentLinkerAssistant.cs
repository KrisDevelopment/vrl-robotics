﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ParentLinkerAssistant : EditorWindow {
	
	private static Vector2 DIMENSION_MIN = new Vector2(250, 250);
	
	[MenuItem("VRL Tools/Linker Assistant")]
	public static void Show () {
		
		EditorWindow win = GetWindow<ParentLinkerAssistant>("Parent Linker Assistant");
		win.minSize = DIMENSION_MIN;
	}
	
	void OnGUI () {
		//pick selection
		GameObject selectedObject = Selection.activeGameObject;
		Transform selectedTransform = null;
		if(selectedObject)
			selectedTransform = selectedObject.transform;
		if(selectedTransform){
			if(GUILayout.Button("Link all children")){
				foreach(Transform child in selectedObject.transform){
					Debug.Log("[Parent Linker Assistant] Child Name: " + child.name);
					VRL_ParentLinker _vrlpl = child.gameObject.GetComponent<VRL_ParentLinker>() as VRL_ParentLinker;
					if(!_vrlpl)
						_vrlpl = (VRL_ParentLinker) Undo.AddComponent(child.gameObject, typeof(VRL_ParentLinker));
					
					if(_vrlpl){
						SerializedObject _so = new SerializedObject((Object) _vrlpl);
						SerializedProperty _so_parent = _so.FindProperty("parent");
						_so.Update();
						_so_parent.objectReferenceValue = selectedTransform;
						_so.ApplyModifiedProperties();
					}
				}
			}
			
			if(!CheckChildrenLinks(selectedTransform))
				GUILayout.Label("Some children are not linked!", EditorStyles.helpBox);
		}
	}
	
	bool CheckChildrenLinks (Transform t) {
		bool _ok = true;
		foreach(Transform child in t)
			if(!child.gameObject.GetComponent<VRL_ParentLinker>())
				_ok = false;
			else if(child.gameObject.GetComponent<VRL_ParentLinker>().parent == null)
				_ok = false;
		return _ok;
	}

}
