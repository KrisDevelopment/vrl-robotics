﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAttach : MonoBehaviour {

	public Transform nest;
	public VRL_Module targetModule;
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space))
			targetModule.AttachTo(nest);
	}
}
