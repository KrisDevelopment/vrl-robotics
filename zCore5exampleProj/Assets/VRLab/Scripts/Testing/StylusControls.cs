﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using zSpace.Core.Samples;
using zSpace.Core;

public class StylusControls : MonoBehaviour {
		///////////////////////////////////////////////////////
		// the following piece of code was re factored       //
		// from the StylusObjectManipulationSample provided  //
		// by zSpace Inc.                                    //
		///////////////////////////////////////////////////////
		
		private enum StylusState{
			Idle,
			Grab
		}
		
		private static readonly float DEFAULT_STYLUS_BEAM_WIDTH	 = 0.0002f;
		private static readonly float DEFAULT_STYLUS_BEAM_LENGTH = 0.3f;

		private ZCore		 _zCore = null;
		private bool		 _wasButtonPressed = false;

		private GameObject	 _stylusBeamObject	 = null;
		private LineRenderer _stylusBeamRenderer = null;
		private float		 _stylusBeamLength	 = DEFAULT_STYLUS_BEAM_LENGTH;

		private StylusState	 _stylusState		  = StylusState.Idle;
		private GameObject	 _grabObject		  = null;
		private Vector3		 _initialGrabOffset	  = Vector3.zero;
		private Quaternion	 _initialGrabRotation = Quaternion.identity;
		private float		 _initialGrabDistance = 0.0f;
		
		//----
		private ZCore.Pose pose;
		private VRL.TransformData _grabTransformData = null;
		private VRL.InputInfo inputInfo = new VRL.InputInfo();
		private bool isButtonPressed = false;
		
	void Start(){
		_zCore = GameObject.FindObjectOfType<ZCore>();
		if (_zCore == null)
		{
			Debug.LogError("Unable to find reference to zSpace Core");
			this.enabled = false;
			return;
		}

		// Create the stylus beam's GameObject.
		_stylusBeamObject = new GameObject("StylusBeam");
		_stylusBeamRenderer = _stylusBeamObject.AddComponent<LineRenderer>();
		_stylusBeamRenderer.material = new Material(Shader.Find("Transparent/Diffuse"));
		
		#if UNITY_5_4
			_stylusBeamRenderer.SetColors(Color.white, Color.white);
		#else
			_stylusBeamRenderer.startColor = Color.white;
			_stylusBeamRenderer.endColor = Color.white;
		#endif
	}
	
	void InputGather() {
		//[eventually use this method for platform-specific input handling, should the need to do so arise]
		isButtonPressed = /*double check in case of platform compatibility issues*/
			(inputInfo.primaryHold) ? true : _zCore.IsTargetButtonPressed(ZCore.TargetType.Primary, 0);
	}
	
	void Update()
		{
			// Grab the latest stylus pose and button state information.
			pose = _zCore.GetTargetPose(ZCore.TargetType.Primary, ZCore.CoordinateSpace.World);

			switch (_stylusState)
			{
				case StylusState.Idle:
					{
						_stylusBeamLength = DEFAULT_STYLUS_BEAM_LENGTH;

							// Update the stylus beam length.
							_stylusBeamLength = inputInfo.hitInfo.distance / _zCore.ViewerScale;

							// If the front stylus button was pressed, initiate a grab.
							if (isButtonPressed && !_wasButtonPressed)
							{
								// Begin the grab.
								_grabTransformData.Set(inputInfo.grabObject.transform);
								this.BeginGrab(inputInfo.grabObject, inputInfo.hitInfo.distance, pose.Position, pose.Rotation);
								_stylusState = StylusState.Grab;
							}
					}
					break;

				case StylusState.Grab:
					{
						// Update the grab.
						this.UpdateGrab(pose.Position, pose.Rotation);

						// End the grab if the front stylus button was released.
						if (!isButtonPressed && _wasButtonPressed)
						{
							_stylusState = StylusState.Idle;
							_grabTransformData = null;
						}
					}
					break;

				default:
					break;
			}

			// Update the stylus beam.
			this.UpdateStylusBeam(pose.Position, pose.Direction);

		// Cache state for next frame.
		_wasButtonPressed = isButtonPressed;
	}
	
	
	 public void BeginGrab(GameObject hitObject, float hitDistance, Vector3 inputPosition, Quaternion inputRotation)
		{
			Vector3 inputEndPosition = inputPosition + (inputRotation * (Vector3.forward * hitDistance));

			// Cache the initial grab state.
			_grabObject			 = hitObject;
			_initialGrabOffset	 = Quaternion.Inverse(hitObject.transform.rotation) * (hitObject.transform.position - inputEndPosition);
			_initialGrabRotation = Quaternion.Inverse(inputRotation) * hitObject.transform.rotation;
			_initialGrabDistance = hitDistance;
		}

		private void UpdateGrab(Vector3 inputPosition, Quaternion inputRotation)
		{
			Vector3 inputEndPosition = inputPosition + (inputRotation * (Vector3.forward * _initialGrabDistance));
			
			if(!_grabTransformData) //init
				_grabTransformData = new VRL.TransformData();
			
			// Update the grab object's rotation.
			Quaternion objectRotation = inputRotation * _initialGrabRotation;
			//MODIFY READABLE TRANSFORMDATA INSTEAD OF A GAME OBJECT
			_grabTransformData.rotation = objectRotation;

			// Update the grab object's position.
			Vector3 objectPosition = inputEndPosition + (objectRotation * _initialGrabOffset);
			//MODIFY READABLE TRANSFORMDATA INSTEAD OF A GAME OBJECT
			_grabTransformData.position = objectPosition;
		}

		private void UpdateStylusBeam(Vector3 stylusPosition, Vector3 stylusDirection)
		{
			if (_stylusBeamRenderer != null)
			{
				float stylusBeamWidth  = DEFAULT_STYLUS_BEAM_WIDTH * _zCore.ViewerScale;
				float stylusBeamLength = _stylusBeamLength * _zCore.ViewerScale;

			#if UNITY_5_4
				_stylusBeamRenderer.SetWidth(stylusBeamWidth, stylusBeamWidth);
			#else
				_stylusBeamRenderer.startWidth = stylusBeamWidth;
				_stylusBeamRenderer.endWidth = stylusBeamWidth;
			#endif
				_stylusBeamRenderer.SetPosition(0, stylusPosition);
				_stylusBeamRenderer.SetPosition(1, stylusPosition + (stylusDirection * stylusBeamLength));
			}
		}
	
	//---------------------------
	public void MouseInput (VRL.InputInfo inputInfo) { //IN
		this.inputInfo = inputInfo;
	}
	
	public VRL.TransformData GetPoseInfo (){ //OUT
		return ((pose != null) ? VRL.TransformData.ZPoseToTransformData(pose) : null);
	}
	
	public VRL.TransformData GetGrabData (){ //OUT
		return _grabTransformData;
	}
}
