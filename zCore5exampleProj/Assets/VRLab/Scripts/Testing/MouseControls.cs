﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(StylusControls))]

//BUILD ERROR: (MouseControls.cs) _ray value is used outside of the editor-compiled code
public class MouseControls : MonoBehaviour {

	public Camera mainCamera;
	public GameObject oDebugMarker;
	public LayerMask rayMask;
	public StylusControls stylusControls;
	public static GameObject
		moduleToSpawn = null,
		modulePreview = null;
	public static VRL_Module moduleForAttach = null;
	private static Transform dragObject;
	private static VRL_Output lastOutput;
	private static float
		cursorForward = 0.1f,
		mouseDownTime = 0f;
	private static Vector3 lastCursorPos = Vector3.zero;
	private VRL.InputInfo inputInfo = new VRL.InputInfo();
	
	void Start () {
		if(!mainCamera){
			mainCamera = Camera.main; //try main camera tag
			if(!mainCamera) //if it still failed
				mainCamera = GetComponent<Camera>() as Camera; //try the current object
		}
		
		if(!stylusControls)
			stylusControls = GetComponent<StylusControls>();
	}
	void Update () {
		if(!mainCamera)
			return;
		
		float _clickTimer = 0.2f;
		
		RaycastHit _hit;
		bool
			_hitb = false,
			_mouseUp = false,
			_primaryButton = Input.GetMouseButton(0),
			_primaryClick = Input.GetMouseButtonDown(0),
			_secondaryClick = false,
			_middleClick = Input.GetMouseButtonDown(2);
			
		#if UNITY_EDITOR
			Ray _ray = mainCamera.ScreenPointToRay(Input.mousePosition);
			_hitb = (Physics.Raycast(_ray, out _hit, 99f, rayMask));
		#else
			VRL.TransformData _stylusTrDt = stylusControls.GetPoseInfo();
			_hitb = (Physics.Raycast(_stylusTrDt.position, _stylusTrDt.rotation * Vector3.forward, out _hit, 99f, rayMask));
		#endif
		
		_mouseUp = Input.GetMouseButtonUp(0);
		_secondaryClick = Input.GetMouseButtonDown(1);
		
		VRL_ParentLinker _foundLinker = null;
		VRL_Input _hitInput = null;
		Transform _hitObject = null;
		Vector3 _moduleSpawnPoint = Vector3.zero;
		Quaternion _moduleSpawnRot = Quaternion.identity;
		VRL_Module _hitMdl = null;
		
		if(_hitb){
			_hitObject = _hit.transform;
			_foundLinker = _hitObject.GetComponent<VRL_ParentLinker>();
			_hitInput = _hitObject.GetComponent<VRL_Input>() as VRL_Input;
			_moduleSpawnPoint = _hit.point + _hit.normal * 0.02f;
			
			if(_primaryClick || _secondaryClick || _middleClick){
				Transform _pgo = _hit.transform;
				if(_foundLinker)
					_pgo = _foundLinker.GetParent();
				if(_pgo)
					_hitMdl = _pgo.GetComponent(typeof(VRL_Module)) as VRL_Module;
			}
			
			if(moduleToSpawn){
				if(!modulePreview){
					modulePreview = Instantiate(moduleToSpawn, Vector3.zero, _moduleSpawnRot);
					VRL_Module _mdl = modulePreview.GetComponent(typeof(VRL_Module)) as VRL_Module;
					if(_mdl)
						_mdl.MakePreview(true);
				}
				modulePreview.transform.position = _moduleSpawnPoint;
			}
		}
		
		if(_primaryButton){ //primary button HOLD ------
			mouseDownTime += Time.deltaTime;
			
			if(_hitb) {
				if(moduleToSpawn){
					Instantiate(moduleToSpawn, _moduleSpawnPoint, _moduleSpawnRot);
					moduleToSpawn = null;
					if(modulePreview){
						Destroy(modulePreview);
						modulePreview = null;
					}
				}
				else{
					if(mouseDownTime >=_clickTimer){
						if(!dragObject){
							if(_foundLinker)
								dragObject = _foundLinker.GetParent();
							else if(_hitObject.tag != "Immovable Object")
								dragObject = _hitObject;
							
							if(dragObject)
								if(dragObject.GetComponent<VRL_Module>())
									dragObject.GetComponent<VRL_Module>().Movable(true);
						}
					}
				}
			}
		}else if(Menu.IsMenu(VRL.Menues.Editor)){
			if(_mouseUp && mouseDownTime < _clickTimer){
				if(_hitb){
					if(!SceneManager.MakingTransition() && !SceneManager.MakingAttachment())
						lastOutput = _hitObject.GetComponent<VRL_Output>() as VRL_Output;
					
					if(lastOutput){
						SceneManager.MakingTransition(true);
						if(_hitInput){
							lastOutput.Link(_hitInput);
							SceneManager.MakingTransition(false);
							lastOutput = null;
						}
					}
				}
			}
			
				if(dragObject)
					if(dragObject.GetComponent<VRL_Module>())
						dragObject.GetComponent<VRL_Module>().Movable(false);
				dragObject = null;
				
			mouseDownTime = 0;
		}
		
		if(_primaryClick){ //primary button CLICK ------
			if(_hitb){
			Debug.Log("HIT: " + _hit.transform.name);
				if(moduleForAttach){
					if(_hit.transform.tag == "Nest"){
						moduleForAttach.AttachTo(_hit.transform);
						moduleForAttach = null;
					}
				}
			}
		}
		
		if(_secondaryClick){ //secondary button ------
			if(SceneManager.MakingTransition()){
				if(lastOutput)
					lastOutput.ClearLink();
				SceneManager.MakingTransition(false);
				lastOutput = null;
			}
			if(_hitMdl)
				_hitMdl.Interact();
		}else if(_middleClick){ //middle button ------
			if(_hitMdl){
				_hitMdl.Attachable(true);
			}
		}
		
		//cursor positioning
		Vector3 _cursorPos = _ray.direction * cursorForward;
		if(_hitb){
			_cursorPos = _hit.point;
			cursorForward = Vector3.Distance(mainCamera.transform.position, _cursorPos);
		}
		cursorForward += Input.GetAxis("Mouse ScrollWheel") * 0.1f;
		
		oDebugMarker.transform.position = _cursorPos;
		Vector3 _cursorDirection = _cursorPos - lastCursorPos;
		lastCursorPos = _cursorPos;
		
		if(lastOutput){
			lastOutput.DrawOutput(_cursorPos);
		}
		
		
		//input information - stylus controls communication
		inputInfo.hit = _hitb;
		inputInfo.primaryClick = _primaryClick;
		inputInfo.primaryHold = _primaryButton;
		inputInfo.secondaryClick = _secondaryClick;
		inputInfo.middleClick = _middleClick;
		inputInfo.hitInfo = _hit;
		if(dragObject)
			inputInfo.grabObject = dragObject.gameObject;
		stylusControls.MouseInput(inputInfo);
		
		if(dragObject && Menu.IsMenu(VRL.Menues.Editor)){ //let the telekinesis begin!
			#if UNITY_EDITOR
				dragObject.transform.position += _cursorDirection;
			#else
				VRL.TransformData _stylusGrabData = stylusControls.GetGrabData();
				if(_stylusGrabData){
					dragObject.transform.position = _stylusGrabData.position;
					dragObject.transform.rotation = _stylusGrabData.rotation;
				}
			#endif
		}
		
		if(Input.GetButtonDown("Cancel")){
			if(moduleForAttach){
				moduleForAttach.Attachable(false);
				moduleForAttach = null;
			}
		}
	}
}
