﻿namespace VRL {
	public interface iListener {
		void Signal ();
		void Signal (bool b);
		void Signal (float f);
		void Signal (string str);
		void Signal (VRLVal v);
	}
	
	public interface iNode {
		void SetNodeInformation(NodeInformation ni);
		NodeType GetNodeType();
		UnityEngine.GameObject GetGameObject();
		void Delete();
	}
	
	public class VRLVal {
		public string stringVal;
		public float floatVal;
		public bool boolVal;
		
		public VRLVal () {
			stringVal = "";
			floatVal = 0f;
			boolVal = false;
		}
		
		public VRLVal (string str, float f, bool b) {
			stringVal = str;
			floatVal = f;
			boolVal = b;
		}
		
		public VRLVal(VRLVal val){
			stringVal = val.stringVal;
			floatVal = val.floatVal;
			boolVal = val.boolVal;
		}
	}
	
	[System.Serializable]
	public class TransformData{
		public UnityEngine.Vector3 position;
		public UnityEngine.Quaternion rotation;
		
		public TransformData () {
			position = UnityEngine.Vector3.zero;
			rotation = UnityEngine.Quaternion.identity;
		}
		
		public TransformData (UnityEngine.Vector3 position, UnityEngine.Quaternion rotation){
			this.position = position;
			this.rotation = rotation;
		}
		
		public TransformData (UnityEngine.Transform tr){
			position = tr.position;
			rotation = tr.rotation;
		}
		
		public TransformData (TransformData dt){
			position = dt.position;
			rotation = dt.rotation;
		}
		
		public void Set (UnityEngine.Transform tr){
			position = tr.position;
			rotation = tr.rotation;
		}
		
		public void DebugLog(){
			DebugLog(this);
		}
		
		public static implicit operator bool(TransformData trdt) {
			return (trdt != null);
		}
		
		public static void DebugLog(TransformData trdt){
			UnityEngine.Debug.Log("(TransformData) Position: " + trdt.position + " Rotation: " + trdt.rotation);
		}
		
		//ZSpace specific
		public static TransformData ZPoseToTransformData(zSpace.Core.ZCore.Pose pose){
			return new TransformData(pose.Position, pose.Rotation);
		}
	}
	
	public class InputInfo { //use this class for communication between the Mouse Input script and the Stylus Control one
		public bool
			primaryClick = false,
			primaryHold = false,
			secondaryClick = false,
			secondaryHold = false,
			middleClick = false,
			middleHold = false,
			hit = false;
		public UnityEngine.RaycastHit hitInfo;
		public UnityEngine.GameObject grabObject = null;
	}
	
	[System.Serializable]
	public class NodeInformation {
		public string name = "Default Node";
		public UnityEngine.Vector3 position = UnityEngine.Vector3.zero;
		public int[] inputs, outputs;
		public NodeType nodeType;
		public string variableData = ""; //[@var_ID:VALUE]
		
		public NodeInformation (){
			name = "Default Node";
			position = UnityEngine.Vector3.zero;
			inputs = new int[0];
			outputs = new int[0];
			nodeType = NodeType.Constant;
		}
		
		public NodeInformation (NodeInformation ni){
			name = ni.name;
			position = ni.position;
			inputs = ni.inputs;
			outputs = ni.outputs;
			nodeType = ni.nodeType;
		}
		
		public void StoreVariable (int index, string value){
			int _vdlen = variableData.Length; //char length of the data field
			System.Collections.Generic.List<UnityEngine.Vector2> _indexes = new System.Collections.Generic.List<UnityEngine.Vector2>(); //(start, end)

			int _counter = 0;
			if(_vdlen > 6){ //read existing data and determine variable positions
				for(int i = 0; i < _vdlen; i++){
					if(variableData.Substring(i, 4) == "@var"){
						if(_counter < _indexes.Count){
							_indexes[i] = new UnityEngine.Vector2(_indexes[i].x, i);
						}
						_indexes.Add(new UnityEngine.Vector2(i, _vdlen - i)); //assign the remaining difference (to avoid having to assign the y val of the last object from the list)
						
						_counter++;
					}
				}
			}
			//TODO: recording new variables in the string and operate with the _ID of the variables
		}
		
		public string RetrieveVariable (int index) {
			return variableData;
		}
	}
	
	[System.Serializable]
	public class NodeData {
		public NodeInformation[] nodeInformation;
		
		public NodeData () {
			nodeInformation = new NodeInformation[0];
		}
		
		public NodeData (NodeData nd) {
			nodeInformation = new NodeInformation[0];
			nodeInformation = nd.nodeInformation;
		}
		
		public NodeInformation GetNodeInformation (int id){
			if(nodeInformation != null)
				if(nodeInformation.Length < id)
					return nodeInformation[id];
				
			UnityEngine.Debug.Log("[NodeData ERROR] Node Information access error: Node ID exceeds the node array!");
			return null;
		}
	}
	
	
	public enum NodeType{
		BoolCompare,
		FloatCompare,
		FloatAdd,
		FloatSubtract,
		StringAdd,
		Constant,
		SineFunction
	}
	
	public enum DriveInput{
		Forward,
		Sideways
	}
	
	public enum Menues {
		Editor,
		Pause,
		Options,
		About,
		NodeEditor
	}
}