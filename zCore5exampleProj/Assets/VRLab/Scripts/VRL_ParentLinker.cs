﻿using UnityEngine;

public class VRL_ParentLinker : VRL_ObjectBase {
	public Transform parent;
	
	private VRL_Module moduleScript = null;
	
	public Transform GetParent() {
		return parent;
	}
	
	void Start () {
		base.OnStart();
		if(parent){
			moduleScript = parent.GetComponent(typeof(VRL_Module)) as VRL_Module;
		}else{
			Debug.Log("[VRL_ParentLinker] No parent transform! Object: " + gameObject.name);
		}
		
		if(gameObject.tag == "Nest" && GetComponent<Rigidbody>() == null){
			Rigidbody _r = (Rigidbody) gameObject.AddComponent(typeof(Rigidbody));
			_r.isKinematic = true;
		}
	}
	
	void Update () {
		if(moduleScript){
			if(moduleScript.IsPreview())
				if(!base.isPreview)
					base.MakePreview(true);
		}
	}
}
