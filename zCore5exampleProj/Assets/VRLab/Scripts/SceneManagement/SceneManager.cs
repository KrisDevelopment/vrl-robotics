﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour {
	private static bool PLAYING = false;
	private static bool TRANSITION = false;
	private static bool ATTACHMENT = false;

	public static void Play () {
		PLAYING = true;
		//TODO: record objects and toggle kinematic on the parent
	}
	
	public static void Stop () {
		PLAYING = false;
		//TODO: reset objects
	}
	
	public static bool IsPlaying (){
		return PLAYING;
	}
	
	//runtime editor
	public static bool MakingTransition (){
		return TRANSITION;
	}
	
	public static void MakingTransition (bool b){
		TRANSITION = b;
	}
	
	public static bool MakingAttachment(){
		return ATTACHMENT;
	}
	
	public static void MakingAttachment (bool b){
		ATTACHMENT = b;
	}
}
