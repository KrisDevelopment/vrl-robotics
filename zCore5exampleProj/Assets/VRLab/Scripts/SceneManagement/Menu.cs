﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {
	private static Menu instance;
	
	//___________
	public MenuGroups[] menuGroups;
	public UnityEngine.UI.Text playTextUI;
	public UnityEngine.UI.RawImage[] moduleIcons;
	public Canvas canvas;
	
	private int
		index = 0,
		pModuleButtonsOffset = 0;
		
	private int[] sortedMenuGroups;
	private string modulesLibPath = "VRL/Modules";
	private GameObject[] pModulesLibrary;
	private Texture2D[] pModuleIcons;
	
	private bool editor = false;
	
	void Awake() {
		instance = this;
		
		#if UNITY_EDITOR
		editor = true;
		#endif
		
		sortedMenuGroups = new int[VRL.Menues.GetNames(typeof(VRL.Menues)).Length];
		SortMenuGroups();
		
		//load modules
		Object[] _loadedModules = Resources.LoadAll(modulesLibPath, typeof(GameObject));
		pModulesLibrary = new GameObject[_loadedModules.Length];
		for(int i = 0; i < _loadedModules.Length; pModulesLibrary[i] = (GameObject) _loadedModules[i], i++);
		
		pModuleIcons = new Texture2D[pModulesLibrary.Length];
		for(int i = 0; i < _loadedModules.Length; pModuleIcons[i] =  pModulesLibrary[i + pModuleButtonsOffset].GetComponent<VRL_Module>().icon, i++);
		
		PlatformSpecificOperations();
	}
	
	void PlatformSpecificOperations(){
		if(editor){
			canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		}else{
			canvas.renderMode = RenderMode.ScreenSpaceCamera;
		}
	}
	
	void Update () {
		for(int i = 0; i < menuGroups.Length; i++){
			if(sortedMenuGroups[i] >= 0 && sortedMenuGroups[i] < menuGroups.Length){
				GameObject _groupParent = menuGroups[sortedMenuGroups[i]].groupParent;
				if(_groupParent)
					_groupParent.SetActive(index == i);
			}
		}
		for(int i = 0; i< moduleIcons.Length; i++){
			moduleIcons[i].texture = (i + pModuleButtonsOffset < pModulesLibrary.Length) ? pModuleIcons[i + pModuleButtonsOffset] : null;
		}
		
		playTextUI.text = (SceneManager.IsPlaying()) ? "PLAY MODE" : "EDIT MODE";
		
		if(menuGroups[sortedMenuGroups[index]].menu != VRL.Menues.NodeEditor)
			if(VRL_NodeSpace.isOpen)
				VRL_NodeSpace.Hide();
				
		//input
		if(Input.GetButtonDown("Cancel")){
			if(!MouseControls.moduleForAttach) //prevent the menu from showing if the user just wants to deselect a module
				CancelButton();
		}
	}
	
	public void PlayButton () {
		if(!SceneManager.IsPlaying())
			SceneManager.Play();
	}
	
	public void CancelButton () { 
		if(SceneManager.IsPlaying()){
			SceneManager.Stop();
		}
		else{
			if(index != (int)VRL.Menues.Editor)
				index = (int)VRL.Menues.Editor;
			else
				index = (int)VRL.Menues.Pause;
		}
	}
	
	public void ModuleButton (int buttonId){
		int _selectedModule = buttonId + pModuleButtonsOffset;
		if(_selectedModule < pModulesLibrary.Length)
			MouseControls.moduleToSpawn = pModulesLibrary[_selectedModule];
	}
	
	public void ModuleOffsetButton (int dir){
		pModuleButtonsOffset += dir;
		if(pModuleButtonsOffset < 0)
			pModuleButtonsOffset = 0;
	}
	
	
	public static void QuitToDesktop () {
		Application.Quit();
	}
	
	public static void SetIndexByName (string menuName) {
		instance.index = (int) VRL.Menues.Parse(typeof(VRL.Menues), menuName, true);
	}
	
	public static void SetIndex (VRL.Menues menu) {
		instance.index = (int) menu;
	}
	
	public static void SetIndex(int i) {
		instance.index = i;
	}
	
	public static int GetIndex() {
		return instance.index;
	}
	
	public static bool IsMenu (VRL.Menues mn){
		if(mn == null){
			Debug.Log("[ERROR] VRL.Menues null!");
			return false;
		}
		return (instance.menuGroups[instance.sortedMenuGroups[instance.index]].menu == mn);
	}
	
	private void SortMenuGroups () {
		for(int i = 0; i < sortedMenuGroups.Length; i++){
			sortedMenuGroups[i] = -1;
			for(int k = 0; k < menuGroups.Length; k++){
				if(i == (int)menuGroups[k].menu)
					sortedMenuGroups[i] = k;
			}
		}
	}
	
	[System.Serializable]
	public class MenuGroups {
		public GameObject groupParent;
		public VRL.Menues menu;
	}
}
