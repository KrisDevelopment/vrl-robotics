﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRL;

public class VRL_Output : MonoBehaviour, iListener {

	public VRL_Input linkedInput;
	private string wirePath = "VRL/Wire";
	private Vector3 cursorPos = Vector3.zero;
	private bool drawOutput = false;
	private GameObject oWire;
	private LineRenderer lrWire;
	
	public void Signal () {
		if(linkedInput == null) return;
		linkedInput.Signal();
	}
	
	public void Signal (bool b){
		if(linkedInput == null) return;
		linkedInput.Signal(b);
	}
	
	public void Signal (float f){
		if(linkedInput == null) return;
		linkedInput.Signal(f);
	}
	
	public void Signal (string str){
		if(linkedInput == null) return;
		linkedInput.Signal(str);
	}
	
	public void Signal (VRLVal v){
		if(linkedInput == null) return;
		linkedInput.Signal(v);
	}
	
	public Vector3 GetPosition(){
		return transform.position;
	}
	
	public void DrawOutput (Vector3 pos){
		drawOutput = true;
		cursorPos = pos;
	}
	
	public void Link (VRL_Input inp){
		linkedInput = inp;
	}
	
	public void ClearLink(){
		linkedInput = null;
	}
	
	private void EnableWire (bool b){
		if(!oWire){
			oWire = Instantiate(Resources.Load<GameObject>(wirePath), transform.position, Quaternion.identity);
			oWire.transform.parent = transform;
			lrWire = oWire.GetComponent<LineRenderer>() as LineRenderer;
		}
		oWire.SetActive(b);
	}
	
	void LateUpdate(){
		bool _drawWire = false;
		if(drawOutput){
			if(lrWire){
				lrWire.SetPosition(0, transform.position);
				lrWire.SetPosition(1, cursorPos);
				_drawWire = true;
			}
		}
		else if(linkedInput && lrWire){
			lrWire.SetPosition(0, transform.position);
			lrWire.SetPosition(1, linkedInput.GetPosition());
			_drawWire = true;
		}
		
		EnableWire(_drawWire);
		drawOutput = false; //reset this so if DrawOutput is not called again next frame the connection wire doesn't appear if the linked input is empty
	}
}
