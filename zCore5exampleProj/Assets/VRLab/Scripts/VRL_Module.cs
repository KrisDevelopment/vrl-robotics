﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRL;

public class VRL_Module : VRL_ObjectBase {
	public Texture2D icon;
	public VRL_Output[] outputs; //medium for passing information
	public Transform mountPin; //used to determine mount position
	
	void Start(){
		base.OnStart();
		if(GetComponent<Rigidbody>())
			this.wasKinematic = GetComponent<Rigidbody>().isKinematic;
	}
	
	public void AttachTo (Transform nest) {
		//error checks
		if(!nest){
			Debug.Log("[ERROR] Null nest!");
			return;
		}
		
		if(!mountPin){
			Debug.Log("[ERROR] Trying to attach object " + gameObject.name + " to " + nest.name + " but no mount pin was found!");
			return;
		}
		
		transform.BroadcastMessage("MakeKinematic", true, SendMessageOptions.DontRequireReceiver);
		if(GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().isKinematic = true;
		
		//make the nest parent
		Quaternion _rotationDifference = VRLu.GetRotationOffset(transform.rotation, mountPin.rotation);
		transform.rotation = _rotationDifference * Quaternion.LookRotation(nest.forward, nest.up); //do the rotation first so the offset vector is correct
		transform.position = nest.position + (transform.position - mountPin.position);
		transform.parent = nest;
		
		Attachable(false);
		transform.BroadcastMessage("CollisionToggle", false, SendMessageOptions.DontRequireReceiver);
		transform.BroadcastMessage("MakeKinematic", true, SendMessageOptions.DontRequireReceiver);
	}
	
	public void Detach () {
		transform.parent = null;
		transform.BroadcastMessage("CollisionToggle", true, SendMessageOptions.DontRequireReceiver);
		transform.BroadcastMessage("MakeKinematic", false, SendMessageOptions.DontRequireReceiver);
	}
	
	public void Movable (bool b) {
		transform.BroadcastMessage("MakeMovable", b, SendMessageOptions.DontRequireReceiver);
		MakeMovable(b);
	}
	
	public void Attachable (bool b) {
		if(!mountPin){
			Debug.Log("[VRL_Module WARNING] Can't make module attachable - mountPin is null!");
			return;
		}
		
		if(b){
			transform.BroadcastMessage("MakePreview", true, SendMessageOptions.DontRequireReceiver);
			MouseControls.moduleForAttach = this;
		}else{
			Debug.Log("EXITING PREVIEW");
			transform.BroadcastMessage("MakePreview", false, SendMessageOptions.DontRequireReceiver);
		}
	}
	
	public virtual void InputVal (VRLVal val, int inputId) { Debug.Log("[InputVal WARNING] Using default Input method!"); }
	public virtual void Interact () {}
}
